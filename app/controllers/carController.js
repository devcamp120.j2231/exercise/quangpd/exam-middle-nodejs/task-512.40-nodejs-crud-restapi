//Import thư viện mongoose
const mongoose = require("mongoose");

//Import user model
const userModel = require("../modules/userModel");
//Import car model 
const carModel = require("../modules/carModels");

//Get All Car
const getAllCar = (req,res) =>{
    //B1 : thu thập dữ liệu
    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về 
    console.log("Get All Car");
    carModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error ${err.message}`
            })
        }else{
            return res.status(200).json({
                message : `Load All Car Successfully!`,
                cars : data
            })
        }
    })
}

//Get Car by Id 
const getCarById = (req,res) =>{
    //B1 : Thu thập dữ liệu
    let carId = req.params.carId;
    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(carId)){
        return res.status(400).json({
            message :`Bad Request - Car Id is not valid`
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    carModel.findById(carId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message : `Internal server error ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Get Car By Id Successfully !`,
                car : data
            })
        }
    })
}

//Create Car
const createCar = (req,res) =>{
    //B1 : Thu thập dữ liệu
    let userId = req.params.userId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message :`Bad Request - User Id is not valid`
        })
    }

    if(!body.model){
        return res.status(400).json({
            message :`Bad Request - model is required`
        })
    }

    if(!body.vId){
        return res.status(400).json({
            message :`Bad Request - vId is required`
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    let newCar = new carModel({
        _id : mongoose.Types.ObjectId(),
        model : body.model,
        vId : body.vId
    });

    carModel.create(newCar,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }

        //Khi tạo xong car cần thêm id car mới vào mảng car của user
        userModel.findByIdAndUpdate(userId,{
            $push : {cars : data._id}
        },(err1,data1) =>{
            if(err1){
                return res.status(500).json({
                    message : `Internal server error`
                })
            }else{
                return res.status(201).json({
                    message :`Create Car Successfully !`,
                    data : data
                })
            }
        }
        )
    })


}

//Get all car of user
const getAllCarOfUser = (req,res) =>{
    //B1 : thu thập dữ liệu
    let userId = req.params.userId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message :`Bad Request - user id is not valid`
        })
    }

    //B3 : Xư lý và trả về kết quả 
    userModel.findById(userId).populate("cars").exec((err,data)=>{
        if(err){
            return res.status(500).json({
                message :`Internal server error ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Get All Car Of User Successfully !`,
                cars : data.cars
            })
        }
    })
}

//Update Car
const updateCarById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let carId = req.params.carId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(carId)){
        return res.status(400).json({
            message :`Bad request - carId is not valid`
        })
    }

    if(!body.model){
        return res.status(400).json({
            message :`Bad Request - model is required`
        })
    }

    if(!body.vId){
        return res.status(400).json({
            message :`Bad Request - vId is required`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    let updateCar = new carModel({
        model : body.model,
        vId : body.vId
    })
    carModel.findByIdAndUpdate(carId,updateCar,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Update Car Successfully`,
                car : data
            })
        }
    })


}

//Delete Car
const deleteCarById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let userId = req.params.userId;
    let carId = req.params.carId;
    
    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message :`Bad Request - userId is not valid`
        })
    }

    if(!mongoose.Types.ObjectId.isValid(carId)){
        return res.status(400).json({
            message :`Bad Request - CarId is not valid`
        })
    }



    //B3 : Xử lý và trả về dữ liệu
    carModel.findByIdAndDelete(carId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error ${err.message}`
            })
        }

        userModel.findByIdAndUpdate(userId,{
            $pull :{cars :carId}
        },(err1,data1) =>{
            if(err1){
                return res.status(500).json({
                    message :`Internal server error : ${err1.message}`
                })
            }else{
                return res.status(204).send()
            }
        })
    
    })
}

    
module.exports = {getAllCar , getCarById , createCar , updateCarById , deleteCarById , getAllCarOfUser}