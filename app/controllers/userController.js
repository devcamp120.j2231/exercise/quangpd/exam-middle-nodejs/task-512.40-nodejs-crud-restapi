//Import thư viện mongoose
const mongoose = require("mongoose");

//Import user Model
const userModel = require("../modules/userModel");

//get all user
const getAllUser = (req,res) =>{
    //B1 : thu thập dữ liệu

    //B2 : kiểm tra dữ liệu

    //B3 : Xử lý và trả về kết quả 
    console.log("Get All User");
    userModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal Server Error :${err.message}`
            })
        }else{
            return res.status(200).json({
                message : `Load All User Successfully`,
                user : data
            })
        }
    })
}

//get User By User Id
const getUserById = (req,res) =>{
    //B1 : Thu thập dữ liệu
    let userId = req.params.userId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message : `Bad Request - UserId is not valid`
        })
    }

    //B3 : Xử lý và trả về kết quả
    userModel.findById(userId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Load User By Id Successfully !`,
                user : data
            })
        }
    })
}

//Create User 
const createUser = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!body.name){
        return res.status(400).json({
            message : `Bad request - Name is required`
        })
    }

    if(!body.phone){
        return res.status(400).json({
            message : `Bad request - phone is required`
        })
    }

    if(!Number(body.age)){
        return res.status(400).json({
            message : `Bad request - age is number`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    let newUser = new userModel({
        _id : mongoose.Types.ObjectId(),
        name : body.name,
        phone : body.phone,
        age : body.age
    });

    userModel.create(newUser,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal Server Error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New User Successfully !`,
                user : data
            })
        }
    })

}

//Update User
const updateUserById = (req,res) =>{
    //B1 : Thu thập dữ liệu
    let userId = req.params.userId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message :`Bad Request - User Id is not valid`
        })
    }

    if(!body.name){
        return res.status(400).json({
            message : `Bad request - Name is required`
        })
    }

    if(!body.phone){
        return res.status(400).json({
            message : `Bad request - phone is required`
        })
    }

    if(!Number(body.age)){
        return res.status(400).json({
            message : `Bad request - age is number`
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    let updateUser = new userModel({
        name : body.name,
        phone : body.phone,
        age : body.age
    });

    userModel.findByIdAndUpdate(userId,updateUser,(err,data)=>{
        if(err){
            return res.status(500).json({
                message :`Internal Server Error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Update User By Id Successfully !`,
                user : data
            })
        }
    })
}

//Delete User
const deleteUserById = (req,res) =>{
    //B1 : Thu thập dữ liệu
    let userId = req.params.userId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message :`Bad Request - userId is not valid`
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    userModel.findByIdAndDelete(userId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message :`Internal server error ${err.message}`
            })
        }else{
            return res.status(204).json({
                message : `Delete user by user Id Successfully !`,
                user :data
            })
        }
    })
}
//Export thành module
module.exports = { getAllUser , getUserById , createUser , updateUserById , deleteUserById}