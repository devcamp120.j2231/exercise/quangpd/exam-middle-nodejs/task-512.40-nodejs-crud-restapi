//Khai báo thư viện express
const express = require("express");
//Import controller
const { getAllCar, getCarById, createCar, updateCarById, deleteCarById, getAllCarOfUser } = require("../controllers/carController");

//Import MiddleWare
const { carMiddleWare, getAllCarMiddleWare, getCarByIdMiddleWare, createCarMiddleWare, updateCarByIdMiddleWare, deleteCarByIdMiddleWare, getAllCarOfUserMiddleWare } = require("../middleware/carMiddleWare");

//Khai báo router
const carRouter = express.Router();

//MiddleWare dùng chung cho cả router
carRouter.use(carMiddleWare);

//get All Car
carRouter.get("/cars",getAllCarMiddleWare,getAllCar);

//get Car by id 
carRouter.get("/cars/:carId",getCarByIdMiddleWare,getCarById);

//Create New Car
carRouter.post("/users/:userId/cars",createCarMiddleWare,createCar);

//Get All car of User
carRouter.get("/users/:userId/cars",getAllCarOfUserMiddleWare,getAllCarOfUser);

//Update Car by Id
carRouter.put("/cars/:carId",updateCarByIdMiddleWare,updateCarById);

//Delete Car by Id
carRouter.delete("/users/:userId/cars/:carId",deleteCarByIdMiddleWare,deleteCarById);

module.exports = { carRouter }