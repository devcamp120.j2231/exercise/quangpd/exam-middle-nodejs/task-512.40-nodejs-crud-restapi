//Khai báo thư viện express
const express = require("express");
//Import controller
const { getAllUser, getUserById, createUser, updateUserById, deleteUserById } = require("../controllers/userController");

//Import middleWare
const { userMiddleWare, getAllUserMiddleWare, getUserByIdMiddleWare, createNewUserMiddleWare, updateUserMiddleWare, deleteUserMiddleWare } = require("../middleware/userMiddleWare");

//Khai báo router
const userRouter = express.Router();

//MiddleWare chung cho cả router
userRouter.use(userMiddleWare);

//get All user
userRouter.get("/users",getAllUserMiddleWare,getAllUser);

//get user by user Id
userRouter.get("/users/:userId",getUserByIdMiddleWare,getUserById);

//create new user
userRouter.post("/users",createNewUserMiddleWare,createUser);

//update user by user id
userRouter.put("/users/:userId",updateUserMiddleWare , updateUserById);

//delete user by user id
userRouter.delete("/users/:userId",deleteUserMiddleWare,deleteUserById);

//Export module
module.exports = {userRouter};