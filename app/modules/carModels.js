//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//Tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongo
const carSchema = new Schema ({
    _id : mongoose.Types.ObjectId,
    model : {
        type : String,
        required : true
    },
    vId : {
        type : String,
        required : true,
        unique : true
    }
})

//Export schema ra model
module.exports = mongoose.model("car",carSchema);