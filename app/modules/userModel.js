//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//Tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongo
const userSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    name : {
        type : String,
        required : true
    },
    phone : {
        type : String,
        required : true,
        unique : true
    },
    age : {
        type : Number,
        default :0
    },
    cars : [{
        type : mongoose.Types.ObjectId,
        ref : 'car'
    }]
},
{
    timestamps : true   
})

//Export schema ra model
module.exports = mongoose.model("user",userSchema);