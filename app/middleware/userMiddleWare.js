//MiddleWare Dùng chung cho user
const userMiddleWare = (req,res,next) =>{
    console.log("User MiddleWare - Time : " + new Date() + "Method :" + req.method);
    next();
}

//MiddleWare get All User
const getAllUserMiddleWare = (req,res,next) =>{
    console.log("Get All User MiddleWare");

    next();
}

//MiddleWare get User By id
const getUserByIdMiddleWare = (req,res,next) =>{
    console.log("Get User By Id MiddleWare");

    next();
}

//MiddleWare Create New User
const createNewUserMiddleWare = (req,res,next) =>{
    console.log("Create New User MiddleWare");

    next();
}

//MiddleWare Update User
const updateUserMiddleWare = (req,res,next) =>{
    console.log("Update User MiddleWare");

    next();
}

//MiddleWare Delete User
const deleteUserMiddleWare = (req,res,next) =>{
    console.log("Delete User MiddleWare");

    next();
}
module.exports = {userMiddleWare , getAllUserMiddleWare ,getUserByIdMiddleWare , createNewUserMiddleWare ,updateUserMiddleWare ,deleteUserMiddleWare};