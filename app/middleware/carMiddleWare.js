//MiddleWare Dùng chung cho car
const carMiddleWare = (req,res,next) =>{
    console.log("Car MiddleWare - Time : " + new Date() + "Method :" + req.method);
    next();
}

//MiddleWare Get All Car
const getAllCarMiddleWare = (req,res,next) =>{
    console.log("Get All Car MiddleWare ");

    next();
}

//MiddleWare Get Car By Id
const getCarByIdMiddleWare = (req,res,next) =>{
    console.log("Get Car By Id MiddleWare ");

    next();
}

//MiddleWare Create Car
const createCarMiddleWare = (req,res,next) =>{
    console.log("Create Car MiddleWare");

    next();
}

//MiddleWare Get All Car Of user
const getAllCarOfUserMiddleWare = (req,res,next) =>{
    console.log("Get All Car Of User MiddleWare ");

    next();
}

//MiddleWare Update Car by id
const updateCarByIdMiddleWare = (req,res,next) =>{
    console.log("Update Car By Car Id MiddlWare");

    next();
}

//MiddleWare Delete Car by id 
const deleteCarByIdMiddleWare = (req,res,next) =>{
    console.log("Delete Car by Id MiddleWare");

    next();
}

module.exports = { carMiddleWare , getAllCarMiddleWare , getCarByIdMiddleWare,createCarMiddleWare, updateCarByIdMiddleWare , deleteCarByIdMiddleWare , getAllCarOfUserMiddleWare}