//Khai báo thư viện express
const express = require("express");

//Khai báo mongoose
const mongoose = require("mongoose");

//Khai báo các model mongoose
const userModel = require("./app/modules/userModel");
const carModel = require("./app/modules/carModels");


//Khai báo các router
const { userRouter } = require("./app/routes/userRouter");
const { carRouter } = require("./app/routes/carRouter");



//Khởi tạo app
const app = express();

//Khai báo cổng
const port = 8000;

//Khai báo sử dụng json
app.use(express.json());

//Kết nối CSDL (sử dụng mongoose theo phiên bản 6x)
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_UserCar",(error) =>{
    if(error) throw error;
    console.log("Connect Successfully ! ");
});

app.use("/",userRouter);
app.use("/",carRouter);

app.listen(port, () =>{
    console.log("App listening on port :" ,port);
})